#include <windows.h>
#include <GL/glut.h>
#include <iostream>
#include <Ball.h>
#include <Lifebuoy.h>
#include <Box.h>
#include <Aquarium.h>

using namespace std;

int slices = 50;
int stacks = 50;
GLfloat y_pos = 1.5f;
GLfloat x_pos = 0;
GLfloat z_pos = -6.0f;
GLfloat gravity = 0.000001f;
GLfloat speed = 0;
bool falling = false;
double a = 0;
GLfloat max_z = -5.0f;
GLfloat test = 1.0f;
GLfloat pin_y = -0.5f;
GLfloat pin_z = -9.0f;
GLfloat potential = 0.0006f;
GLfloat radius = 0.3f;
GLfloat side = 0.5f;
GLfloat camera_z = 0.1f;
GLfloat camera_y = 0.8f;
GLfloat eyes_y = 0.5f;
GLfloat lz = -0.1;
GLfloat zoom = 0.0f;
float time = 0;
GLfloat weight = 0.01f;
bool object_ball;
bool object_lifebuoy;
bool object_box;
GLfloat mass = 0;
GLfloat mass2 = 0;
GLfloat still_y = 0;
bool submerge = 0;
bool simulationStart = false;
GLfloat waterLevel = -0.8f;
bool waterLevel2 = waterLevel;
GLfloat boundary = waterLevel+0.1;

void renderH1(std::string str, GLfloat px, GLfloat py, int r, int g, int b){
    int l = str.size(), i;

    glPushMatrix();
        glRasterPos3f(px, py, z_pos);
        for(i = 0; i < l; i++){
            glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, str[i]);
        }
    glPopMatrix();
}

void renderP(std::string str, GLfloat px, GLfloat py, int r, int g, int b){
    int l = str.size(), i;
    float x = 0.0f;
    glPushMatrix();
        glRasterPos3f(px, py, 0);
        for(i = 0; i < l; i++){
            glutBitmapCharacter(GLUT_BITMAP_8_BY_13, str[i]);
            x += 0.048f;
        }
}

void initGL() {
    glClearColor(189/255.0f, 225/255.0f, 247/255.0f, 1.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0, camera_y, camera_z,
              0.0, eyes_y, -1.0,
              0.0, 0.5f, 0.0);

    if (falling) {
        speed = speed - gravity;
        y_pos = y_pos + speed;
    }


    if (y_pos < waterLevel) {
        speed = 0;
        speed = speed + potential;
        if (potential > 0)
            potential -= 0.0002;
        else {
            potential = 0;
            speed = 0;
            gravity = 0;
            falling = false;
            submerge = true;
        }

    }
    if (y_pos < boundary) {
        waterLevel -= speed*2;
        if (speed == 0){
            if (mass > 0) {
                waterLevel = boundary + mass;
                mass -= 0.001;
            }
        }
    }

//    if (y_pos < boundary && submerge && mass2 > 0.01 && (object_ball || object_box) && (y_pos-radius > -2.0f)) {
//        y_pos = y_pos - 0.0001;
//    }


    glColor3d(1,0,0);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

//    glColor3d(0,0,0);
//    glPushMatrix();
//        glTranslated(0,-1.9f,-6.0);
//        glutSolidSphere(0.04,50,50);
//    glPopMatrix();

    if (object_ball) {
        Ball ball;
        glColor4f(1, 1, 1, 0.3);
        ball.setValues(radius, x_pos, y_pos, z_pos);
        ball.renderBall();
    }

    if (object_lifebuoy) {
        Lifebuoy lb;
        glColor4f(1, 1, 1, 0.3);
        lb.setValues(radius, x_pos, y_pos, z_pos);
        lb.renderLifebuoy();
    }

    if (object_box) {
        Box box;
        glColor4f(1, 1, 1, 0.8);
        box.setValues(radius, x_pos, y_pos, z_pos);
        box.set_side(side);
        box.renderBox();
    }

    Aquarium aquarium;
    aquarium.setWaterLevel(waterLevel);
    aquarium.renderAquarium();
    glutSwapBuffers();
}

void reshape(GLsizei width, GLsizei height) {
    if (height == 0) height = 1;
    GLfloat aspect = (GLfloat)width / (GLfloat)height;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, aspect, 0.1f, 100.0f);
}

void idle(void){
    glutPostRedisplay();
}

void resetDefaults(){
    slices = 50;
    stacks = 50;
    y_pos = 1.5f;
    x_pos = 0;
    z_pos = -6.0f;
    gravity = 0.000001f;
    speed = 0;
    falling = false;
    a = 0;
    max_z = -5.0f;
    test = 1.0f;
    pin_y = -0.5f;
    pin_z = -9.0f;
    potential = 0.0006f;
    radius = 0.3f;
    side = 0.5f;
    camera_z = 0.1f;
    camera_y = 0.8f;
    eyes_y = 0.5f;
    lz = -0.1;
    zoom = 0.0f;
    time = 0;
    weight = 0.01f;
    object_ball = false;
    object_lifebuoy = false;
    object_box = false;
    GLfloat mass = 0;
    GLfloat still_y = 0;
    submerge = 0;
    simulationStart = false;
    GLfloat waterLevel = -0.8f;
    waterLevel2 = waterLevel;
}

static void key(unsigned char key, int x, int y){
    if (key == 32) {
        falling = true;
    }
    if (key == 's') {
        resetDefaults();
    }
    if (key == '-') {
        camera_z -= lz;
    }
    if (key == '=') {
        camera_z += lz;
    }

    if (key == '1') {
        object_ball = true;
    }

    if (key == '2') {
        object_lifebuoy = true;
    }

    if (key == '3') {
        object_box = true;
    }


    glutPostRedisplay();
}

void specialKey(int key, int x, int y){
    glutSetKeyRepeat(1);

    switch(key) {
    case GLUT_KEY_UP:
        if (radius < 0.6){
            radius += 0.005;
            side += 0.005;
            mass += 0.005;
            mass2 += 0.005;
        }
        break;
    case GLUT_KEY_DOWN:
        if (radius > 0.2){
            radius -= 0.005;
            side -= 0.005;
            mass -= 0.005;
            mass2 -= 0.005;
        }
        break;
    }
}


void mouse(int button, int state, int x, int y) {
    float xClick = (float)x/GLUT_WINDOW_X;
    float yClick = (float)y/GLUT_WINDOW_Y;
}


void timer(int){
    glutPostRedisplay();
    glutTimerFunc(1000.0/60.0, timer, 0);
}

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };


int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE);
    glutInitWindowSize(600, 600);
    glutInitWindowPosition(50, 50);
    glutCreateWindow("Falling and Floating Simulation");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    glutKeyboardFunc(key);
    glutSpecialFunc(specialKey);
    glutMouseFunc(mouse);
    glutTimerFunc(1000.0/60.0, timer, 0);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);

    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

    initGL();
    glutMainLoop();
    return 0;
}
