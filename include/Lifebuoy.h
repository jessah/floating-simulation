#ifndef LIFEBUOY_H
#define LIFEBUOY_H
#include <GL/glut.h>

class Lifebuoy
{
    public:
        Lifebuoy();
        virtual ~Lifebuoy();
        GLfloat get_x();
        GLfloat get_y();
        GLfloat get_z();
        void set_x(GLfloat);
        void set_y(GLfloat);
        void set_z(GLfloat);
        void set_radius(GLfloat);
        void renderLifebuoy();
        void setValues(GLfloat, GLfloat, GLfloat, GLfloat);

    protected:
        GLfloat x, y, z;
        GLfloat radius;
        int slices, stacks;

    private:
};

#endif // LIFEBUOY_H
