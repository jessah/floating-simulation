#ifndef BALL_H
#define BALL_H
#include <GL/glut.h>


class Ball
{
    public:
        Ball();
        virtual ~Ball();
        GLfloat get_x();
        GLfloat get_y();
        GLfloat get_z();
        void set_x(GLfloat);
        void set_y(GLfloat);
        void set_z(GLfloat);
        void set_radius(GLfloat);
        void renderBall();
        void setValues(GLfloat, GLfloat, GLfloat, GLfloat);

    protected:
        GLfloat x, y, z;
        GLfloat radius;
        int slices, stacks;

    private:
};

#endif // BALL_H
