#ifndef BOX_H
#define BOX_H
#include <GL/glut.h>

class Box
{
    public:
        Box();
        virtual ~Box();
        GLfloat get_x();
        GLfloat get_y();
        GLfloat get_z();
        void set_x(GLfloat);
        void set_y(GLfloat);
        void set_z(GLfloat);
        void set_side(GLfloat);
        void renderBox();
        void setValues(GLfloat, GLfloat, GLfloat, GLfloat);


    protected:
        GLfloat x, y, z;
        GLfloat side;

    private:
};

#endif // BOX_H
