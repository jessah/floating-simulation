#ifndef AQUARIUM_H
#define AQUARIUM_H
#include <GL/glut.h>

class Aquarium
{
    public:
        Aquarium();
        virtual ~Aquarium();
        void renderAquarium();
        void setWaterLevel(GLfloat);
    protected:
        GLfloat waterLevel;
    private:
};

#endif // AQUARIUM_H
