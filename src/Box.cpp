#include "Box.h"

Box::Box()
{
    //ctor
}

Box::~Box()
{
    //dtor
}
void Box::setValues(GLfloat radius, GLfloat x, GLfloat y, GLfloat z){
    this->side = side;
    this->x = x;
    this->y = y;
    this->z = z;
}
GLfloat Box::get_x(){
    return x;
}
GLfloat Box::get_y(){
    return y;
}
GLfloat Box::get_z(){
    return z;
}
void Box::set_x(GLfloat x){
    this->x = x;
}
void Box::set_y(GLfloat y){
    this->y = y;
}
void Box::set_z(GLfloat z){
    this->z = z;
}
void Box::set_side(GLfloat side){
    this->side = side;
}
void Box::renderBox(){
    glPushMatrix();
        glTranslated(x,y,z);
        glRotated(0,0,0,0);
        glutSolidCube(side);
    glPopMatrix();
}
