#include "Ball.h"

Ball::Ball()
{
    this->slices = 50;
    this->stacks = 50;
}

Ball::~Ball()
{
    //dtor
}
void Ball::setValues(GLfloat radius, GLfloat x, GLfloat y, GLfloat z){
    this->radius = radius;
    this->x = x;
    this->y = y;
    this->z = z;
}
GLfloat Ball::get_x(){
    return x;
}
GLfloat Ball::get_y(){
    return y;
}
GLfloat Ball::get_z(){
    return z;
}
void Ball::set_x(GLfloat x){
    this->x = x;
}
void Ball::set_y(GLfloat y){
    this->y = y;
}
void Ball::set_z(GLfloat z){
    this->z = z;
}
void Ball::set_radius(GLfloat radius){
    this->radius = radius;
}
void Ball::renderBall(){
    glPushMatrix();
        glTranslated(x,y,z);
        glutSolidSphere(radius,slices,stacks);
    glPopMatrix();
}
