#include "Lifebuoy.h"

Lifebuoy::Lifebuoy()
{
    this->slices = 50;
    this->stacks = 50;
}

Lifebuoy::~Lifebuoy()
{
    //dtor
}
void Lifebuoy::setValues(GLfloat radius, GLfloat x, GLfloat y, GLfloat z){
    this->radius = radius;
    this->x = x;
    this->y = y;
    this->z = z;
}
GLfloat Lifebuoy::get_x(){
    return x;
}
GLfloat Lifebuoy::get_y(){
    return y;
}
GLfloat Lifebuoy::get_z(){
    return z;
}
void Lifebuoy::set_x(GLfloat x){
    this->x = x;
}
void Lifebuoy::set_y(GLfloat y){
    this->y = y;
}
void Lifebuoy::set_z(GLfloat z){
    this->z = z;
}
void Lifebuoy::set_radius(GLfloat radius){
    this->radius = radius;
}
void Lifebuoy::renderLifebuoy(){
    glPushMatrix();
        glTranslated(x,y,z);
        glRotated(0,5,0,0);
        glutSolidTorus(radius,0.8,slices,stacks);
    glPopMatrix();
}
