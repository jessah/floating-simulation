#include "Aquarium.h"

Aquarium::Aquarium()
{
    //ctor
}

Aquarium::~Aquarium()
{
    //dtor
}
void Aquarium::renderAquarium()
{
    glBegin(GL_QUADS);
    glColor4f(90/255.0, 101/255.0, 104/255.0, 1);
    glVertex3f( 2.0f, 0.0f, -7.0f);
    glVertex3f(-2.0f, 0.0f, -7.0f);
    glVertex3f(-2.0f,  -2.0f, -7.0f);
    glVertex3f( 2.0f,  -2.0f, -7.0f);

    glColor4f(34/255.0, 34/255.0, 34/255.0, 1);
    glVertex3f(-2.0f, 0.0f, -7.0f);
    glVertex3f(-2.0f,  -2.0f, -7.0f);
    glVertex3f(-2.0f,  -2.0f, -5.0f);
    glVertex3f(-2.0f,  0.0f, -5.0f);

    glColor4f(34/255.0, 34/255.0, 34/255.0, 1);
    glVertex3f(2.0f, 0.0f, -7.0f);
    glVertex3f(2.0f,  -2.0f, -7.0f);
    glVertex3f(2.0f,  -2.0f, -5.0f);
    glVertex3f(2.0f,  0.0f, -5.0f);

    glColor4f(82/255.0, 116/255.0, 145/255.0, 1);
    glVertex3f(-2.0f,  -2.0f, -7.0f);
    glVertex3f( 2.0f,  -2.0f, -7.0f);
    glVertex3f( 2.0f,  -2.0f, -5.0f);
    glVertex3f(-2.0f,  -2.0f, -5.0f);

    glColor4f(63/255.0, 97/255.0, 126/255.0, 0.5f);
    glVertex3f( 2.0f, waterLevel, -5.0f);
    glVertex3f(-2.0f, waterLevel, -5.0f);
    glVertex3f(-2.0f,  -2.0f, -5.0f);
    glVertex3f( 2.0f,  -2.0f, -5.0f);

    // top
    glColor4f(82/255.0, 116/255.0, 145/255.0, 0.5f);
    glVertex3f(-2.0f,  waterLevel, -7.0f);
    glVertex3f( 2.0f,  waterLevel, -7.0f);
    glVertex3f( 2.0f,  waterLevel, -5.0f);
    glVertex3f(-2.0f,  waterLevel, -5.0f);

    glColor4f(90/255.0, 101/255.0, 104/255.0, 0.3f);
    glVertex3f( 2.0f, 0.0f, -5.0f);
    glVertex3f(-2.0f, 0.0f, -5.0f);
    glVertex3f(-2.0f,  -2.0f, -5.0f);
    glVertex3f( 2.0f,  -2.0f, -5.0f);
    glEnd();
}
void Aquarium::setWaterLevel(GLfloat waterLevel)
{
    this->waterLevel = waterLevel;
}
